<? include jobs/header ?>

**Your daily business**

- Manage social media content calendar
- Develop meaningful, engaging social media content and campaigns for all of our products and eyeo on Facebook, Twitter, YouTube, Snapchat, Instagram, Google +, and other relevant channels
- Engage with followers, respond appropriately and quickly to users, and provide a solution whenever possible
- Track effectiveness of social media campaigns by analysing the metrics
- Find solutions for our users by facilitating internal communications


**You need for this role**

- Knowledge about  the major social media platforms
- Experience in creating & implementing successful strategic social media campaigns
- Experience in building relationships with users on a variety of social media channels
- Strong grammatical, proofreading, writing and communication skills
- Experience with graphic design, photo editing, and video creation
(recommended)
- Understanding of eyeo products and company strategy
- BS in Communications, Marketing, Business, New Media or Public Relations



<? include jobs/footer ?>
