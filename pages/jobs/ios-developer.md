<? include jobs/header ?>

**Your daily business**

- Being part of a small team developing and maintaining iOS & macOS applications alongside shared libraries
- Working with both Objective-C & Swift codebases
- Taking part in peer code reviews
- Providing an input into future product directions
- Assisting with App Store submissions

**What we are expecting**

- Very good English communication skills
- The ability to work both independently and as part of a small team
- Staying current with the evolution of Apple developer technologies
- An understanding of Apple Human Interface Guidelines & App Store Submission Guidelines
- Familiarity of the Safari Extensions model
- Solid understanding of how the web works
- Experience with Python and/or Javascript would be a plus

<? include jobs/footer ?>
