<? include jobs/header ?>

**Your daily business**

- Perform own exploratory testing against our products to gain familiarity with the products and their similarities and differences
- Run checklist tests against our products upfront releases
- Respond to requests from and perform testing for developers and staff
- Review new tickets from our community, attempt to improve and reproduce the issues and confirm/reject their tickets
- Test recently closed issues on our issue tracker
- Create and keep up to date useful documentation and notes relating to the product and its testing
- Keep up to date with the company/business by reading the internal forums, discussions, email and meeting notes

**What we are expecting**

- Independent but team-oriented work style
- Very good communication skills in English
- Extensive experience with software testing
- Having worked with test automation would be a huge plus, especially not only at website-testing but with native Windows and mobile apps
- Ability to multi-task and adapt to different situations

If this looks / sounds / feels like you, please get in contact with your CV/resume, a short summary of why you’re applying, your earliest possible start date and your salary expectations.

<? include jobs/footer ?>
