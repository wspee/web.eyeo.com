**We are a small but growing company**

Adblock Plus, for now our primary product, is already the most popular browser extension ever. Our goal of making online advertising a more sustainable for consumers and web providers still needs a lot of work though. So while we’re way past the development stage on the one hand, we’re still short of where we want to go.

And we don’t want to stop at Adblock Plus and better advertising! We have a ton of ideas, some already on their way, that just need the right mix of people to make them world- and industry-changers. To do so …

**We need people with conviction**

We’re well aware that we threaten long-established paradigms by asking if the behemoth of online advertising can change its ways. You’ll fit in here if you’re not easily intimidated and you believe that the Internet is not a static, but rather a developing space made for and in the hands of its users (who aren’t mere “consumers”).

<? include jobs/why-gallery ?>

**We don’t get bored here at eyeo, but you don’t have to live here to work here**

The English writer Samuel Johnson once said that, “When a man is tired of London, he is tired of life.” We’d say the same about Cologne and Berlin. Not all of us are from here; in all, our <? include size-of-team ?> people represent 24 countries, and we haven’t yet found a way to get bored — at work or after.

That being said, about half of us work from home — no matter where that home may be. We’re happy to let you work from wherever you’re most comfortable.

When we work, in Cologne, in Berlin or remotely, everyone has an equal spot at the table. New ideas and comments are always welcome, and roles evolve beyond restrictive, unimaginative “titles.” The hierarchy is flat and the politics get left at door. When we’re not working, we play some fiercely competitive ping pong, we grill, drink beer, bowl and, as of recently, race go-carts.
